package customControls.windows;

import customControls.backendToFrontend.MatrixInputPanel;
import essential.Settings;
import singeltons.Console;
import singeltons.MatrixList;

import javax.swing.*;
import java.awt.*;

/*
 *      Window to enter a Matrix and Name into and add that to the MatrixList
 */
public class AddMatrixWindow extends JFrame {

    private final MainWindow owner;
    private JPanel mainPanel;
    private JButton addMatrix;
    private MatrixInputPanel matrixArea;
    private JTextField matrixName;

    public AddMatrixWindow(final MainWindow owner) {
        super("Add Matrix");
        this.owner = owner;
        fillMainPanel();
        setEvents();
        add(mainPanel);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setVisible(true);
        setResizable(false);
        pack();
    }

    private void setEvents() {
        addMatrix.addActionListener(e -> matrixArea.tryGetInputResult().ifPresent(matrix -> {
                if(!matrixName.getText().isEmpty())
                    Console.instance().add(MatrixList.instance().addDefaultWarning(matrixName.getText(), matrix, this) ? "matrix added" : "matrix not added");
                else {
                    Console.instance().add("enter a name in the Add Matrix window");
                    matrixName.setBackground(Settings.alertColor);
                }
            })
        );
    }

    private void fillMainPanel() {
        //construct components
        mainPanel = new JPanel();
        mainPanel.setBackground(Settings.defaultBackground);
        addMatrix = new JButton ("Add");
        matrixArea = new MatrixInputPanel(110, 5, 200, 200);
        matrixName = new JTextField (5);

        //adjust size and set layout
        mainPanel.setPreferredSize(new Dimension(315, 210));
        mainPanel.setLayout(null);

        //add components
        mainPanel.add(addMatrix);
        mainPanel.add(matrixArea);
        mainPanel.add(matrixName);

        //set component bounds (only needed by Absolute Positioning)
        addMatrix.setBounds(5, 35, 100, 20);
        matrixName.setBounds(5, 5, 100, 25);
    }
}
