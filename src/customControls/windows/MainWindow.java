package customControls.windows;

import customControls.backendToFrontend.TermInputPanel;
import customControls.list.JPanelList;
import customControls.list.JPanelListElement;
import customControls.list.MatrixPairPanel;
import essential.Pair;
import essential.Settings;
import essential.Util;
import math.algorithm.*;
import math.matrix.MathErrorException;
import math.matrix.Matrix;
import singeltons.Console;
import singeltons.MatrixList;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;

public class MainWindow extends JFrame implements PropertyChangeListener {

    private JPanel mainPanel;

    private JTextField termNameInputField;
    private TermInputPanel termInputField;

    private JTextArea solutionMatrix;
    private JButton solve;
    private JButton addMatrix;
    private JButton removeFocusedMatrix;
    private JPanelList addedMatrices;
    private JButton load;
    private JComboBox console;

    private JButton addRandomMatrix;
    private JComboBox randOrId;
    private JTextField xDimField;
    private JTextField yDimField;

    private JComboBox singleMatrixOperationPicker;
    private JButton singleMatrixOperationSolve;
    private JTextField singleMatrixOperationMatrixNameInput;

    public MainWindow() {
        super("Matrix Calculator");

        MatrixList.instance().linkPropertyChangeListener(this);
        Console.instance().linkPropertyChangeListener(this);

        initMainPanel();
        addLabels();
        initUserInputFields();
        initInteractables();
        setEvents();
        setToolTips();

        add(mainPanel);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setVisible(true);
        setResizable(false);
        pack();
    }

    private void initMainPanel() {
        mainPanel = new JPanel();
        mainPanel.setBackground(Settings.defaultBackground);
        mainPanel.setLayout(null);
        mainPanel.setPreferredSize(new Dimension(700, 400));
    }

    private void addLabels() {
        JLabel solutionMatrixNameDescriptor = new JLabel("Solution Matrix Name:");
        JLabel solutionMatrixTermDescriptor = new JLabel("Expression:");
        JLabel matrixListDescriptor = new JLabel("Matrix list:");
        JLabel solutionMatrixDescriptor = new JLabel("Solution:");

        solutionMatrixNameDescriptor.setBounds(5, 5, 150, 20);
        solutionMatrixTermDescriptor.setBounds(5, 30, 150, 20);
        matrixListDescriptor.setBounds(5, 105, 100, 20);
        solutionMatrixDescriptor.setBounds(465, 105, 100, 20);

        mainPanel.add(solutionMatrixNameDescriptor);
        mainPanel.add(solutionMatrixTermDescriptor);
        mainPanel.add(matrixListDescriptor);
        mainPanel.add(solutionMatrixDescriptor);
    }

    private void initUserInputFields() {

        randOrId = new JComboBox(new String[]{"random", "identity"});
        singleMatrixOperationPicker = new JComboBox(new String[]{"gaus", "det", "solve", "transpose", "invert"});
        termNameInputField = new JTextField();
        termInputField = new TermInputPanel(160, 30, 300, 20);
        singleMatrixOperationMatrixNameInput = new JTextField();
        xDimField = new JTextField();
        yDimField = new JTextField();

        randOrId.setBounds(175, 80, 85, 20);
        singleMatrixOperationPicker.setBounds(585, 55, 100, 20);
        termNameInputField.setBounds(160, 5, 100, 20);
        singleMatrixOperationMatrixNameInput.setBounds(480, 30, 100, 20);
        xDimField.setBounds(265, 80, 50, 20);
        yDimField.setBounds(320, 80, 50, 20);

        mainPanel.add(randOrId);
        mainPanel.add(singleMatrixOperationPicker);
        mainPanel.add(singleMatrixOperationMatrixNameInput);
        mainPanel.add(xDimField);
        mainPanel.add(yDimField);
        mainPanel.add(termNameInputField);
        mainPanel.add(termInputField);
    }

    private void initInteractables() {
        JScrollPane solutionMatrixScroll = new JScrollPane();

        singleMatrixOperationSolve = new JButton("Algorithm");
        addRandomMatrix = new JButton("Add");
        console = new JComboBox();
        load = new JButton("Load");
        removeFocusedMatrix = new JButton("Delete");
        solutionMatrix = new JTextArea();
        solve = new JButton("Solve");
        addMatrix = new JButton( "Add Matrix");
        addedMatrices = new JPanelList(5,130, 455, 200);

        singleMatrixOperationSolve.setBounds(480, 55, 100, 20);
        addRandomMatrix.setBounds(110, 80, 60, 20);
        console.setBounds(5, 375, 550, 20);
        load.setBounds(595, 375, 100, 20);
        removeFocusedMatrix.setBounds(5, 335, 100, 20);
        solutionMatrixScroll.setBounds(465, 130, 200, 200);
        solve.setBounds(160, 55, 100, 20);
        addMatrix.setBounds(5, 80, 100, 20);

        solutionMatrixScroll.setViewportView(solutionMatrix);

        mainPanel.add(singleMatrixOperationSolve);
        mainPanel.add(addRandomMatrix);
        mainPanel.add(console);
        mainPanel.add(load);
        mainPanel.add(removeFocusedMatrix);
        mainPanel.add(solutionMatrixScroll);
        mainPanel.add(addedMatrices);
        mainPanel.add(solve);
        mainPanel.add(addMatrix);
    }

    private void setEvents() {
        addMatrix.addActionListener( e -> new AddMatrixWindow(this) );

        removeFocusedMatrix.addActionListener(e -> {
            ArrayList<JPanelListElement> removedElements = addedMatrices.removeFocused();
            for(JPanelListElement element : removedElements)
                if(element instanceof MatrixPairPanel)
                    MatrixList.instance().remove(((MatrixPairPanel)element).getMatrixName());

            refresh();
        });

        singleMatrixOperationSolve.addActionListener(e -> {
            String matrixName = singleMatrixOperationMatrixNameInput.getText();
            String name = termNameInputField.getText();
            Matrix relevantMatrix;
            if(matrixName.equals("") || !MatrixList.instance().keySet().contains(matrixName)) {
                JOptionPane.showMessageDialog(null, "Please enter a valid Matrix");
                return;
            }
            if((relevantMatrix = MatrixList.instance().get(matrixName)) != null) {
                MatrixAlgorithm ma = null;
                switch (singleMatrixOperationPicker.getSelectedItem().toString()) {
                    case "gaus":
                        ma = new Gaus(relevantMatrix);
                        break;
                    case "det":
                        ma = new Determinant(relevantMatrix);
                        break;
                    case "solve":
                        ma = new SolveFor(relevantMatrix);
                        break;
                    case "transpose":
                        ma = new Transpose(relevantMatrix);
                        break;
                    case "invert":
                        ma = new Invert(relevantMatrix);
                        break;
                    default:
                        Console.instance().add("invalid selection");
                        throw new IllegalArgumentException("invalid selection");
                }

                int res = JOptionPane.showConfirmDialog(
                        this,
                        "Do you want to display the step by step solution?",
                        "Step by Step",
                        JOptionPane.YES_NO_OPTION);
                boolean outputLog = (res == JOptionPane.YES_OPTION);

                try {
                    ma.solve(outputLog);
                    new AlgorithmStepDisplayWindow(ma);
                } catch (MathErrorException mee) {
                    Console.instance().add(mee.getMessage());
                }
                solutionMatrix.setText(ma.getResult().toString());
                if(!name.isEmpty())
                    MatrixList.instance().addDefaultWarning(name, ma.getResult(), this);
            }
        });

        solve.addActionListener(e -> {
            if (termNameInputField.getText().equals("")) {
                JOptionPane.showMessageDialog(null , "Please select a name");
                return;
            }
            termInputField.tryGetInputResult().ifPresent(m -> {
                solutionMatrix.setText(m.toString());
                String name = termNameInputField.getText();
                if(!name.isEmpty())
                    Console.instance().add(MatrixList.instance().addDefaultWarning(name, m, this) ? 
                    "solution matrix added" : "solution not matrix added");
            });
        });

        load.addActionListener(e -> {
            File loadDestination = Util.filePickerDialog(this);
            if(loadDestination != null)
                if(!MatrixList.instance().load(loadDestination))
                    JOptionPane.showMessageDialog(this, "Loading failed", "Error!", JOptionPane.ERROR_MESSAGE);
        });

        addRandomMatrix.addActionListener(e -> {

            String nameIntro = null;
            Matrix rndOrId = null;
            int count = 1;
            String xText = xDimField.getText();
            String yText = yDimField.getText();

            try {
                double xDim = Double.parseDouble(xText);
                double yDim = Double.parseDouble(yText);
                if(xDim == 0 || yDim == 0
                    || xDim > 10 || yDim > 10) {
                    JOptionPane.showMessageDialog(null, "Please enter valid dimensions (0 < dim < 11)");
                    return;
                }
            } catch (Exception a) {
                JOptionPane.showMessageDialog(null, "Please enter a number");
                return;
            }

            switch(randOrId.getSelectedItem().toString()) {
                case "random":
                    nameIntro = "r";
                    rndOrId = Matrix.generateRandomMatrix(      Integer.parseInt(xDimField.getText()),
                                                                Integer.parseInt(yDimField.getText()), 100 );
                    break;
                case "identity":
                    nameIntro = "id";
                    rndOrId = Matrix.generateIdentityMatrix(    Integer.parseInt(xDimField.getText()),
                                                                Integer.parseInt(yDimField.getText()));
                    break;
                default: throw new IllegalArgumentException("invalid selection");
            }

            for(int i = 0; i < MatrixList.instance().keySet().size(); i++) {
                if(MatrixList.instance().keySet().contains("" + nameIntro + count)) count++;
            }

            MatrixList.instance().addDefaultWarning(
                    nameIntro + count,
                    rndOrId,
                    this
            );
        });

        final MainWindow thisWindow = this;
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if(!MatrixList.instance().isEmpty()) {
                    int res = JOptionPane.showConfirmDialog(
                            thisWindow,
                            "Do you want to save existing Matrices?",
                            "Save?",
                            JOptionPane.YES_NO_OPTION);

                    if(res == JOptionPane.YES_OPTION) {
                        File saveDestination = Util.filePickerDialog(thisWindow);
                        if (saveDestination != null)
                            if(!MatrixList.instance().save(saveDestination))
                                JOptionPane.showMessageDialog(thisWindow, "Saving failed", "Error!", JOptionPane.ERROR_MESSAGE);
                    }
                }

                MatrixList.instance().unlinkPropertyChangeListener(thisWindow);
                Console.instance().unlinkPropertyChangeListener(thisWindow);
                System.exit(0);
            }
        });
    }

    private void refresh() {
        mainPanel.repaint();
        pack();
    }

    private void setToolTips() {
        termNameInputField.setToolTipText("Enter the name for the solution matrix");
        termInputField.setToolTipText("Enter the expression by which the solution matrix is compiled, using real numbers, matrix names and operators");
        addMatrix.setToolTipText("Open a dialog to add a new matrix");
        xDimField.setToolTipText("Dimension x");
        yDimField.setToolTipText("Dimension y");
    }

    private void refreshAddedMatrices() {

        ArrayList<Pair<String, Matrix>> dicConv = MatrixList.instance().toPairList();
        addedMatrices.removeListElements();
        for(Pair<String, Matrix> element : dicConv)
            addedMatrices.add(new MatrixPairPanel(element));

        refresh();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()) {
            case "add":
            case "remove":
                refreshAddedMatrices();
            break;
            case "console message":
                console.addItem(evt.getNewValue());
                console.setSelectedIndex(console.getItemCount() - 1);
            break;
            default: break;
        }
    }
}
