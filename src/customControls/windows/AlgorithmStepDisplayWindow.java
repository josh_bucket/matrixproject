package customControls.windows;

import customControls.list.JPanelList;
import customControls.list.MatrixPairPanel;
import essential.Pair;
import math.algorithm.MatrixAlgorithm;

import javax.swing.*;
import java.awt.*;

/*
 * A window that just contains the individual steps with description and matrix
 */
public class AlgorithmStepDisplayWindow extends JFrame {
    JPanelList stepDisplay;

    public AlgorithmStepDisplayWindow(MatrixAlgorithm a){
        super("Algorithm Display");
        stepDisplay = new JPanelList(0, 0, 700, 200);

        if(!a.readableLog()) {
            dispose();
            return;
        }

        for(int i = 0; i < a.getChangeLog().size(); i++){
            MatrixPairPanel p = new MatrixPairPanel(new Pair<>(a.getChangeLog().get(i).toString(), a.getSteps().get(i)));
            stepDisplay.add(p);
        }

        setPreferredSize(new Dimension(700, 200));
        add(stepDisplay);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setVisible(true);
        pack();
    }
}
