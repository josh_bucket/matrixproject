package customControls.backendToFrontend;

import essential.Settings;
import math.matrix.Matrix;
import math.term.TermInterpreter;

import javax.swing.*;
import java.util.Optional;

/*
 *      This provides the connection between user and TermInterpreter
 */
public class TermInputPanel extends UserInputPanel<JTextField, TermInterpreter, Matrix> {

    public TermInputPanel(int x, int y, int dx, int dy) {
        super(x, y, dx, dy, new JTextField());
    }

    @Override
    protected void addToPanel() {
        add(super.input);
    }

    @Override
    protected TermInterpreter parseInput() throws Throwable {
        return new TermInterpreter(super.input.getText());
    }

    @Override
    protected void leaveFailureMode() {
        super.input.setForeground(Settings.defaultFont);
    }

    @Override
    protected void enterFailureMode(Throwable ex) {
        System.out.println(ex.getMessage());
        super.input.setForeground(Settings.alertColor);
    }

    @Override
    public Optional<Matrix> tryGetInputResult() throws NullPointerException {
        try {
            return Optional.ofNullable(super.inputParser.result());
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Please enter a valid term");
            return Optional.empty();
        }
    }
}
