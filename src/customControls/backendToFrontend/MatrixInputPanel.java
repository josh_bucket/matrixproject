package customControls.backendToFrontend;

import essential.Settings;
import math.matrix.Matrix;

import javax.swing.*;
import java.util.Optional;

/*
 *      This provides the connection between user and Matrix
 */
public class MatrixInputPanel extends UserInputPanel<JTextArea, Matrix, Matrix> {

    public MatrixInputPanel(int x, int y, int dx, int dy) {
        super(x, y, dx, dy, new JTextArea());
    }

    @Override
    protected void addToPanel() {
        add(new JScrollPane(super.input));
    }

    @Override
    protected Matrix parseInput() throws Throwable {
        return Matrix.generateMatrixFromString(super.input.getText());
    }

    @Override
    protected void leaveFailureMode() {
        super.input.setForeground(Settings.defaultFont);
    }

    @Override
    protected void enterFailureMode(Throwable ex) {
        super.input.setForeground(Settings.alertColor);
    }

    @Override
    public Optional<Matrix> tryGetInputResult() {
        return Optional.ofNullable(super.inputParser);
    }
}
