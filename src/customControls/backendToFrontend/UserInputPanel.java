package customControls.backendToFrontend;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.util.Optional;

/*
 *      This provides a connection between JTextComponents and some BackendComponent with its output
 */
public abstract class UserInputPanel<JInputComponent extends JTextComponent, BackendComponent, BackendComponentOutput> extends JPanel {

    protected JInputComponent input;
    protected BackendComponent inputParser;
    protected boolean inputFailureMode;

    protected UserInputPanel(int x, int y, int dx, int dy, JInputComponent input) {
        super();
        this.input = input;
        inputParser = null;
        inputFailureMode = false;

        input.getDocument().addDocumentListener(new CompileInputOnTextChange(this));

        setBounds(x, y, dx, dy);
        setLayout(new BorderLayout());

        addToPanel();
    }

    public abstract Optional<BackendComponentOutput> tryGetInputResult();

    protected abstract void addToPanel();

    protected abstract BackendComponent parseInput() throws Throwable;

    protected abstract void leaveFailureMode();

    protected abstract void enterFailureMode(Throwable ex);

    /*
     *      Feeds the input to the backend, if it cant/can be parsed failureMode is entered/leaved if nessesary
     */
    private void updateBackendFromInput() {
        try {
            inputParser = parseInput();

            if(inputFailureMode) {
                inputFailureMode = false;
                leaveFailureMode();
            }
        } catch(Throwable ex) {
            inputParser = null;

            if(!inputFailureMode) {
                inputFailureMode = true;
                enterFailureMode(ex);
            }
        }
    }

    /*
     *      if the input is changed it instantly checks if the new inout is sensible
     */
    private class CompileInputOnTextChange implements DocumentListener {

        private final UserInputPanel owner;

        public CompileInputOnTextChange(final UserInputPanel owner) { this.owner = owner; }

        @Override
        public void insertUpdate(DocumentEvent e) { owner.updateBackendFromInput(); }

        @Override
        public void removeUpdate(DocumentEvent e) { owner.updateBackendFromInput(); }

        @Override
        public void changedUpdate(DocumentEvent e) { }
    }

    public void setToolTipText(final String text) {
        input.setToolTipText(text);
    }
}
