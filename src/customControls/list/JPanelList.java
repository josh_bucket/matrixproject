package customControls.list;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/*
 +      A vertical list for adding JPanels to
 */
public class JPanelList extends JScrollPane {

    private JPanel mainPanel;

    public JPanelList(int x, int y, int dx, int dy) {
        setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        setBounds(x, y, dx, dy);
        setViewportView(mainPanel);
    }

    public void add(JPanelListElement listElement) {
        mainPanel.add(listElement);
    }

    public ArrayList<JPanelListElement> removeFocused() {
        ArrayList<JPanelListElement> retVal = new ArrayList<>();
        for(Component c : mainPanel.getComponents())
            if(c instanceof JPanelListElement)
                if(((JPanelListElement)c).isFocused()) {
                    retVal.add((JPanelListElement)c);
                    mainPanel.remove(c);
                }
        return retVal;
    }

    public void removeListElements() {
        for(Component c : mainPanel.getComponents())
            if(c instanceof JPanelListElement)
                mainPanel.remove(c);
    }
}
