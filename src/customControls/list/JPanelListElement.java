package customControls.list;

import javax.swing.*;

/*
 *      Element for the list has to be focusable
 */
public class JPanelListElement extends JPanel {
    protected boolean focused;

    public boolean isFocused() {
        return focused;
    }
}
