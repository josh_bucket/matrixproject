package customControls.list;

import essential.Pair;
import math.matrix.Matrix;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/*
 *      ListElement that consists of a String and a Matrix to display a Matrix and its name/a message
 */
public class MatrixPairPanel extends JPanelListElement {

    private Pair<String, Matrix> content;

    public MatrixPairPanel(Pair<String, Matrix> source) {
        content = source;
        focused = false;

        JLabel eleName = new JLabel(source.left);
        JTextArea eleMatrix = new JTextArea(source.right.toString());
        eleMatrix.setEditable(false);

        setBackground(Color.lightGray);
        setLayout(new FlowLayout());
        addMouseListener(new ClickBehaviour());
        add(eleName);
        add(eleMatrix);
    }

    /*
     *      Focused by clicking it. Changes background when focused
     */
    private class ClickBehaviour implements MouseListener {
        @Override
        public void mouseClicked(MouseEvent e) {
            setBackground(focused ? Color.lightGray : Color.PINK);
            focused = !focused;
        }
        @Override public void mousePressed(MouseEvent e) {}
        @Override public void mouseReleased(MouseEvent e) {}
        @Override public void mouseEntered(MouseEvent e) {}
        @Override public void mouseExited(MouseEvent e) {}
    }

    public String getMatrixName() {
        return content.left;
    }
}
