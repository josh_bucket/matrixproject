package singeltons;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;

/*
 *      Console to display messages for the user, mainWindow is listening to this
 */
public class Console {
    private static Console unique;
    private ArrayList<String> val;
    private PropertyChangeSupport changer;

    private Console() {
        val = new ArrayList<>();
        changer = new PropertyChangeSupport(this);
    }

    public static Console instance() {
        if(unique == null)
            unique = new Console();
        return unique;
    }

    public void add(String message) {
        val.add(message);
        changer.firePropertyChange("console message", null, message);
    }

    public void linkPropertyChangeListener(PropertyChangeListener l) {
        changer.addPropertyChangeListener(l);
    }

    public void unlinkPropertyChangeListener(PropertyChangeListener l) {
        changer.removePropertyChangeListener(l);
    }

}
