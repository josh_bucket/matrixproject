package singeltons;

import essential.Pair;
import math.matrix.Matrix;
import store.MatrixStore;
import store.StoreException;

import javax.swing.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.function.Function;

/*
 *      stores all the matrices of the program
 *      Doesn't implement iterator because i only need foreach
 */
public class MatrixList {
    private static MatrixList unique = null;
    private static TreeMap<String, Matrix> matrixDic;
    private PropertyChangeSupport changer;

    private MatrixList() {
        matrixDic = new TreeMap<>();
        changer = new PropertyChangeSupport(this);
    }

    public static MatrixList instance() {
        if(unique == null)
            unique = new MatrixList();
        return unique;
    }

    public boolean add(String name, Matrix matrix, Function<Matrix, Boolean> overrideBehaviour) {
        if(!matrixDic.containsKey(name) || overrideBehaviour.apply(matrixDic.get(name))){
            matrixDic.put(name, matrix);
            changer.firePropertyChange("add", null, name);
            return true;
        }
        return false;
    }

    public boolean addDefaultWarning(String name, Matrix matrix, JFrame owner){
        return add(name, matrix, collisionMatrix -> {
            int res = JOptionPane.showConfirmDialog(
                    owner,
                    "You are about to override an already existing matrix. Continue?",
                    "Warning",
                    JOptionPane.YES_NO_OPTION);
            return res == JOptionPane.YES_OPTION;
        });
    }

    public void addNextName(String name, Matrix matrix) {

        if(!matrixDic.containsKey(name)){
            matrixDic.put(name, matrix);
            changer.firePropertyChange("add", null, name);
            return;
        }

        int i = 1;
        while(matrixDic.containsKey(name + i++)) { }
        matrixDic.put(name + i, matrix);
        changer.firePropertyChange("add", null, name + i);
    }

    /*
     *      converts the matrixDic to a Arraylist of pairs, no copy
     */
    public ArrayList<Pair<String, Matrix>> toPairList() {
        ArrayList<Pair<String, Matrix>> list = new ArrayList<>();
        for(String key : matrixDic.keySet())
            list.add(new Pair<>(key, matrixDic.get(key)));
        return list;
    }

    public Set<String> keySet() {
        return matrixDic.keySet();
    }

    public Matrix get(String name) {
        return matrixDic.get(name);
    }

    public Matrix remove(String name) {
        Matrix removeRes = matrixDic.remove(name);
        if(removeRes != null)
            changer.firePropertyChange("remove", null, name);
        return removeRes;
    }

    public Boolean save(File file) {
        try {
            MatrixStore ms = new MatrixStore(file);
            ms.saveFromMatrixList();
            return true;
        } catch (StoreException sEx) {
            Console.instance().add(sEx.getMessage() + "|" + sEx.getCause().getMessage());
            return false;
        }
    }

    public Boolean load(File file) {
        try {
            MatrixStore ms = new MatrixStore(file);
            ms.loadToMatrixList();
            return true;
        } catch (StoreException sEx) {
            Console.instance().add(sEx.getMessage() + "|" + sEx.getCause().getMessage());
            return false;
        }
    }

    public boolean isEmpty() {
        return matrixDic.isEmpty();
    }

    public void linkPropertyChangeListener(PropertyChangeListener l) {
        changer.addPropertyChangeListener(l);
    }

    public void unlinkPropertyChangeListener(PropertyChangeListener l) {
        changer.removePropertyChangeListener(l);
    }

    public void forEach(Consumer<? super Pair<String, Matrix>> action) {
        for(String key : matrixDic.keySet())
            action.accept(new Pair<>(key, matrixDic.get(key)));
    }
}
