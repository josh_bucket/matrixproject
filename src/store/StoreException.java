package store;

public class StoreException extends Exception {
    public StoreException(String message, Throwable cause) {
        super(message, cause);
    }
}
