package store;

import essential.Pair;
import math.matrix.MathErrorException;
import math.matrix.Matrix;
import singeltons.MatrixList;

import java.io.*;
import java.util.ArrayList;

public class MatrixStore {

    private File destination;
    private final String separator = "<-->";

    public MatrixStore(File destination) {
        this.destination = destination;
    }

    public void loadToMatrixList() throws StoreException {
        if(!destination.canRead())
            throw new StoreException("cant read from file", null);

        ArrayList<Pair<String, String>> content = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(this.destination.getAbsoluteFile()))){
            //stores the current element
            Pair<String, StringBuilder> currentPair = new Pair<>(null, new StringBuilder());
            String currentLine;
            //is toggled when a seperator line is read
            boolean record = false;
            while((currentLine = reader.readLine()) != null) {
                if(!record){
                    if(currentPair.left != null)        //record got just switched off and the current element is ready to be stored
                        content.add(new Pair<>(currentPair.left, currentPair.right.toString()));
                    currentPair = new Pair<>(null, new StringBuilder());        //element is stored clear this for the next
                } else if(!currentLine.equals(separator)) {
                    if(currentPair.left == null)
                        currentPair.left = currentLine;     //if the name part of the pair is null it just started recording and the current line is the name
                    else {
                        if(!currentLine.isEmpty()) {    //name is already filled so the current line is part of the matrix
                            currentPair.right.append(currentLine);
                            currentPair.right.append("\n");
                        }
                    }
                }
                if(currentLine.equals(separator))
                    record = !record;       //toggle record
            }
            //the last element is prepared but never stored in the loop
            content.add(new Pair<>(currentPair.left, currentPair.right.toString()));
        } catch (FileNotFoundException fnfEx) {
            throw new StoreException("file doesnt exist", fnfEx);
        } catch(IOException ioEx) {
            throw new StoreException("reading error", ioEx);
        }

        for(Pair<String, String> matrixPairString : content) {
            try {
                MatrixList.instance().addNextName(matrixPairString.left, Matrix.generateMatrixFromString(matrixPairString.right));
            } catch (MathErrorException meEx) {
                throw new StoreException("matrix format corrupted", meEx);
            }
        }
    }

    public void saveFromMatrixList() throws StoreException {
        if(destination.canRead()) destination.delete();
        try {
            destination.createNewFile();
        } catch(IOException ioEx){
            throw new StoreException("cant create file", ioEx);
        }

        if(!destination.canWrite())
            throw new StoreException("cant write to file", null);

        try (PrintWriter writer = new PrintWriter(this.destination.getAbsoluteFile())){
            MatrixList.instance().forEach(p -> { //go through all matrices and add separators, name, matrix separator
                    writer.println(separator);
                    writer.println(p.left);
                    writer.println(p.right);
                    writer.println(separator);
            });
        } catch (FileNotFoundException fnfEx) {
            throw new StoreException("file doesnt exist", fnfEx);
        }
    }

}
