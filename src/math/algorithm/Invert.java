package math.algorithm;

import math.matrix.MathErrorException;
import math.matrix.Matrix;

public class Invert extends MatrixAlgorithm {

    public Invert(Matrix input) {
        super(input);
    }

    /*
     *      adj(A) * A = det(A) * E
     *              =>
     *      (A^-1 exists => A^-1 = adj(A)/det(A))
     */
    @Override
    public void solve() throws MathErrorException {
        Determinant d = new Determinant(input);
        d.solve(isLogging());
        if(d.getResult().tryGetScalar() == 0) {
            throw new MathErrorException("cant invert matrix with determinant of 0");
        }
        changeLog = d.getChangeLog();
        steps = d.getSteps();

        //getting the adjunct matrix transposed
        result = Matrix.loop(input.getXDim(), input.getYDim(), p -> {
            Determinant detMin = new Determinant(input.getMinor(p.left, p.right));
            try {
                detMin.solve(isLogging());
            } catch (MathErrorException meEx) {} //cant happen anyway
           return  Math.pow(-1, p.left + p.right) * detMin.getResult().tryGetScalar();
        });

        addToLog("make matrix from input where (x, y) is -1^(x + y) * det(minor(input, x, y))", result);

        //transpose it and div by det(input)
        double det = 1.0 / d.getResult().tryGetScalar();
        for (int x = 0; x < result.getXDim(); x++) {
            for (int y = 0; y <= x; y++) {
                double temp = result.getAt(x, y);
                result.setAt(x, y, result.getAt(y, x) * det);
                result.setAt(y, x, temp * det);
            }
        }

        addToLog("divide all coordinates by det(input) and transpose the input", result);

        solved = true;
    }
}
