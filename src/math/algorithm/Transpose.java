package math.algorithm;

import math.matrix.Matrix;

public class Transpose extends MatrixAlgorithm {

    public Transpose(Matrix input) {
        super(input);
    }

    @Override
    public void solve() {
        result = input.transpose();
        addToLog("swap the x, and y coordinates of each value in the matrix", result);
        solved = true;
    }
}
