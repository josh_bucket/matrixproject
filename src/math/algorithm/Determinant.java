package math.algorithm;

import math.matrix.MathErrorException;
import math.matrix.Matrix;

/*
 *      Calculates the Determinant
 */
public class Determinant extends MatrixAlgorithm {

    public Determinant(Matrix input) {
        super(input);
    }

    @Override
    public void solve() throws MathErrorException {
        if(!input.isSquare())
            throw new MathErrorException("square matrix required for determinant");

        switch (input.getXDim()) {
            case 1:
                result = new Matrix(input.getAt(0,0));
                addToLog("1x1 determinant is the only value in the matrix", result);
                solved = true;
                return;
            case 2:
                result = new Matrix(input.getAt(0,0) * input.getAt(1,1) - input.getAt(1,0) * input.getAt(0,1));
                addToLog("Solve 2x2 determinant", result);
                solved = true;
                return;
        }

        //this is the laplace formula after the first row
        double det = 0;
        for(int i = 0; i < input.getXDim(); i++) {
            Matrix inputNoRow1NoColI = input.getMinor(0, i);//Matrix.loop(input.getXDim() - 1, input.getYDim() - 1, p -> input.getAt(p.left + 1, p.right + (p.right < finalI ? 0 : 1)));
            Determinant recursiveStep = new Determinant(inputNoRow1NoColI);
            recursiveStep.solve();
            Matrix detMatrixRes = recursiveStep.getResult();
            addToLog("get determinant of minor (0, " + i + ")", detMatrixRes);
            det += detMatrixRes.mult(input.getAt(0, i) * Math.pow(-1, i)).getAt(0, 0);
        }
        result = new Matrix(det);
        addToLog("add all of them together multiplied by their respective sign (-1)^(x + y) where x and y are the minors row/column", result);
        solved = true;
    }
}
