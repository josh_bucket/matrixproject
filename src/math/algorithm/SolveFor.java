package math.algorithm;

import math.matrix.MathErrorException;
import math.matrix.Matrix;

/*
 *      calculates the solution v, Mv=e for a nx(n+1) where the first nxn is M and the last column is e
 */
public class SolveFor extends MatrixAlgorithm {

    public SolveFor(Matrix input) {
        super(input);
    }

    @Override
    public void solve() throws MathErrorException {
        if(input.getXDim() != input.getYDim() - 1)
            throw new MathErrorException("no specific solution");

        /*
         *      Use the Gaus algorithm so that the solution is just putting the elements to the other side
         *      from top to bottom
         */
        Gaus g = new Gaus(input);
        g.solve(isLogging());
        Matrix sol = g.getResult();

        for(int i = 0; i < input.getXDim(); i++) {
            if(sol.getAt(i, i) == 0)
                throw new MathErrorException("no solution");
        }

        changeLog = g.getChangeLog();
        steps = g.getSteps();

        int lastYIndex = sol.getYDim() - 1;
        for(int row = sol.getXDim() - 1; row >= 0; row--)
            for(int col = sol.getYDim() - 2; col >= 0; col--) {
                //get the most far left non zero element in sol
                double current = sol.getAt(row, col);
                if(current == 0) continue;

                //put it on the other side to get the variable value
                double solvedVar = sol.getAt(row, lastYIndex) / current;
                sol.setAt(row, lastYIndex, solvedVar);
                sol.setAt(row, col, 0);
                addToLog("Take the factor before the variable to the other side to solve for the variable", sol);
                for(int i = row - 1; i >= 0; i--){
                    //all elements in this column can now be multiplied with the variable and get put on the other side as well
                    sol.setAt(i, lastYIndex, sol.getAt(i, lastYIndex) - sol.getAt(i, col) * solvedVar);
                    sol.setAt(i, col, 0);
                }
                addToLog("Put the last solved variables into the upper rows", sol);
            }
        //v is now stored where p was stored, extract it to a nx1 matrix
        result = Matrix.loop(sol.getXDim(), 1, p -> sol.getAt(p.left, lastYIndex));
        solved = true;
    }
}
