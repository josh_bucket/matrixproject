package math.algorithm;

import math.matrix.MathErrorException;
import math.matrix.Matrix;

import java.util.ArrayList;

/*
 *      Frontend only really knows this class
 */
public abstract class MatrixAlgorithm {

    protected boolean solved;
    private boolean logging;
    protected Matrix input;
    protected Matrix result;
    protected ArrayList<Matrix> steps;
    protected ArrayList<StringBuilder> changeLog;

    protected MatrixAlgorithm(Matrix input) {
        this(input, false);
    }

    protected MatrixAlgorithm(Matrix input, boolean logging) {
        this.input = input;
        this.logging = logging;
        steps = new ArrayList<>();
        changeLog = new ArrayList<>();
        solved = false;
    }

    public ArrayList<Matrix> getSteps() {
        return steps;
    }

    public ArrayList<StringBuilder> getChangeLog() {
        return changeLog;
    }

    public boolean readableLog(){
        return logging && solved;
    }

    protected void addToLog(String message, Matrix step) {
        if(!logging)
            return;
        addToChangeLog(message);
        addToSteps(step);
    }

    protected void addToChangeLog(String message) {
        changeLog.add(new StringBuilder(message));
    }

    protected void addToSteps(Matrix step) {
        steps.add(step.copy());
    }

    public abstract void solve() throws MathErrorException;

    public void solve(boolean logging) throws MathErrorException {
        this.logging = logging;
        solve();
    }

    public boolean isLogging() {
        return logging;
    }

    public Matrix getResult() {
        return solved ? result : null;
    }
}
