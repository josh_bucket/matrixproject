package math.algorithm;

import math.matrix.MathErrorException;
import math.matrix.Matrix;

public class Gaus extends MatrixAlgorithm {

    public Gaus(Matrix input) {
        this(input, false);
    }

    public Gaus(Matrix input, boolean logging) {
        super(input, logging);
    }

    /*
     *      @return the index of the biggest element in the column vector starting at startRow
     */
    private int getRowIndexForMaxInColumn(int startRow, int column) {
        int max_index = startRow;
        for(int i = startRow + 1; i < input.getXDim(); i++) {
            if(Math.abs(result.getAt(i, column)) > Math.abs(result.getAt(max_index, column)))
                max_index = i;
        }
        return max_index;
    }

    @Override
    public void solve() throws MathErrorException {
        result = input.copy();
        int pivotRow = 0, pivotColumn = 0;
        while(pivotRow < result.getXDim() && pivotColumn < result.getYDim()) {
            addToLog("Find the row that has the biggest number in column " + pivotColumn, result);
            int indexForMaxInColumn = getRowIndexForMaxInColumn(pivotRow, pivotColumn);
            if(result.getAt(indexForMaxInColumn, pivotColumn) == 0) {
                addToLog("Number is already 0, go to next column", result);
                pivotColumn++;
            } else {
                result = result.swapRows(pivotRow, indexForMaxInColumn);
                addToLog("Swap that row to be the current pivot row", result);
                for(int i = pivotRow + 1; i < result.getXDim(); i++) {
                    double f = result.getAt(i, pivotColumn) / result.getAt(pivotRow, pivotColumn);
                    result.setAt(i, pivotColumn, 0);
                    for(int j = pivotColumn + 1; j < result.getYDim(); j++)
                        result.setAt(i, j, result.getAt(i, j) - result.getAt(pivotRow, j) * f);
                    addToLog("Subtract pivot row(" + pivotRow + ") from row " + i + " accordingly with factor: " + f, result);
                }
                pivotRow++; pivotColumn++;
            }
        }
        solved = true;
    }
}
