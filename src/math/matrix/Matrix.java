package math.matrix;

import essential.Pair;
import essential.Util;

import java.util.Random;
import java.util.function.Function;

public class Matrix {

    private double[][] data;
    private final int xDim;
    private final int yDim;

    public Matrix copy(){
        return Matrix.loop(getXDim(), getYDim(), p -> getAt(p.left, p.right));
    }

    public static Matrix generateIdentityMatrix(int dim) {
        if(!checkDimensions(dim, dim))
            throw new IllegalArgumentException();

        return loop(dim, dim, p -> (p.left.equals(p.right) ? 1.0 : 0.0));
    }

    public static Matrix generateIdentityMatrix(int xDim, int yDim) {
        if(!checkDimensions(xDim, yDim))
            throw new IllegalArgumentException();

        return loop(xDim, yDim, p -> (p.left.equals(p.right) ? 1.0 : 0.0));
    }

    /*
     *      bound is upper/lower bound and excluded
     */
    public static Matrix generateRandomMatrix(int xDim, int yDim, int bound){
        if(!checkDimensions(xDim, yDim))
            throw new IllegalArgumentException();

        return loop(xDim, yDim, p -> new Random().nextInt(bound * 100) / 100.0 );
    }

    /*
     *      splits at newlines tabs and space
     */
    public static Matrix generateMatrixFromString(final String matrixAsString) throws NumberFormatException, MathErrorException {

        String[] lines = matrixAsString.lines().toArray(String[]::new);
        int xDim = lines.length;
        int yDim = 0;
        Double[][] numbers = new Double[xDim][yDim];

        for(int i = 0; i < xDim; i++) {
            numbers[i] = Util.ExtendedString.NonRegex.split(lines[i], " \t".toCharArray()).stream()
                    .map(Double::parseDouble)
                    .toArray(Double[]::new);
            int length = numbers[i].length;
            if(yDim == 0)
                yDim = length;
            else if(length != yDim)
                throw new MathErrorException("matrix needs to be rectangular");
        }

        return loop(xDim, yDim, p -> numbers[p.left][p.right]);
    }

    public boolean isSquare(){
        return xDim == yDim;
    }

    public Matrix(double scalar) {
        this(1, 1);
        data[0][0] = scalar;
    }

    public Matrix(int xDim, int yDim) {
        if(!checkDimensions(xDim, yDim))
            throw new IllegalArgumentException();

        this.xDim = xDim;
        this.yDim = yDim;
        data = new double[xDim][yDim];
    }

    //checks

    private boolean checkPos(int x, int y) {
        return xDim > x && x >= 0 && yDim > y && y >= 0;
    }

    private static boolean checkDimensions(int x, int y) {
        return x > 0 && y > 0;
    }

    private static boolean checkDouble(double val) {
        return Double.isFinite(val) && !Double.isNaN(val);
    }

    //get set

    public void setAt(int x, int y, double val) {
        if(!checkPos(x, y))
            throw new IllegalArgumentException();

        if(!checkDouble(val))
            throw  new IllegalArgumentException();

        data[x][y] = val;
    }

    public double getAt(int x, int y) {
        if(!checkPos(x, y))
            throw new IllegalArgumentException();

        return data[x][y];
    }

    public int getXDim(){
        return xDim;
    }

    public int getYDim(){
        return yDim;
    }

    //operations

    public Matrix add(final Matrix rhs) throws MathErrorException {
        if(xDim != rhs.xDim || yDim != rhs.yDim)
            throw new MathErrorException("add/sub: required dimensions: n x m & n x m");

        return loop(xDim, yDim, p -> data[p.left][p.right] + rhs.data[p.left][p.right]);
    }

    public Matrix mult(final Matrix rhs) throws MathErrorException {
        if(yDim != rhs.xDim)
            throw new MathErrorException("mult: required dimensions: n x m & m x l");

        return loop(xDim, yDim, p -> {
            double retVal = 0;
            for(int k = 0; k < yDim; k++)
                retVal += data[p.left][k] * rhs.data[k][p.right];
            return retVal;
        });
    }

    public Matrix mult(double rhs) throws MathErrorException {
        if(!checkDouble(rhs))
            throw new MathErrorException("mult: requires real number");

        return loop(xDim, yDim,p-> rhs * data[p.left][p.right]);
    }

    public Matrix pow(int n) throws MathErrorException {
        if(xDim != yDim)
            throw new MathErrorException("cant pow a matrix that isnt square");
        Matrix retVal = loop(xDim, yDim, p -> data[p.left][p.right]);
        while(--n > 0)
            retVal = retVal.mult(this);

        return retVal;
    }

    public Matrix transpose() {
        return loop(yDim, xDim, p -> data[p.right][p.left]);
    }

    //line/row operations

    public Matrix swapRows(int rowA, int rowB) throws MathErrorException {
        if(!checkPos(rowA, 0) || !checkPos(rowB, 0))
            throw new MathErrorException("invalid rows");

        return loop(xDim, yDim,
                p ->    (p.left == rowA ?  data[rowB][p.right] :
                        (p.left == rowB ?  data[rowA][p.right] :
                                            data[p.left][p.right])));
    }

    public Matrix swapColumns(int colA, int colB) throws MathErrorException {
        if(!checkPos(0, colA) || !checkPos(0, colB))
            throw new MathErrorException("invalid column index");

        return loop(xDim, yDim,
                p ->    (p.right == colA ?  data[p.left][colB] :
                        (p.right == colB ?  data[p.left][colA] :
                                          data[p.left][p.right])));
    }

    public Matrix addRows(int rowA, int rowB) throws MathErrorException {
        if(!checkPos(rowA, 0) || !checkPos(rowB, 0))
            throw new MathErrorException("invalid row index");

        return loop(xDim, yDim,
                p ->    (p.left == rowA ?  data[rowB][p.right] + data[rowA][p.right] :
                                            data[p.left][p.right]));
    }

    public Matrix multRows(int row, double factor) throws MathErrorException {
        if(!checkPos(row, 0))
            throw new MathErrorException("invalid row index");

        return loop(xDim, yDim,
                p ->    (p.left == row ?  factor * data[row][p.right] :
                                            data[p.left][p.right]));
    }

    public Matrix addCols(int colA, int rowB) throws MathErrorException {
        if(!checkPos(0, colA) || !checkPos(0, rowB))
            throw new MathErrorException("invalid column index");

        return loop(xDim, yDim,
                p ->    (p.right == colA ?  data[p.left][colA] + data[p.left][rowB] :
                                            data[p.left][p.right]));
    }

    public Matrix multCols(int col, double factor) throws MathErrorException {
        if(!checkPos(0, col))
            throw new MathErrorException("invalid column index");

        return loop(xDim, yDim,
                p ->    (p.right == col ?  factor * data[p.left][col] :
                                        data[p.left][p.right]));
    }

    public Double tryGetScalar() {
        if(xDim == 1 && yDim == 1)
            return data[0][0];
        return null;
    }

    public Matrix getMinor(int x, int y) {
        if(!checkPos(x, y))
            throw new IllegalArgumentException();

        return Matrix.loop(xDim - 1, yDim - 1, p -> getAt(p.left + (p.left < x ? 0 : 1), p.right + (p.right < y ? 0 : 1)));
    }

    /*
     *      @param fnc calculates the number at (x, y) in the matrix; in the function pair::left := x, pair::right := y
     */
    public static Matrix loop(int xDim, int yDim, final Function<Pair<Integer, Integer>, Double> fnc) {

        Matrix retVal = new Matrix(xDim, yDim);

        for(int x = 0; x < xDim; x++)
            for(int y = 0; y < yDim; y++)
                retVal.data[x][y] = fnc.apply(new Pair<>(x, y));

        return retVal;
    }

    @Override
    public String toString() {
        StringBuilder sB = new StringBuilder();

        for(int x = 0; x < xDim; x++) {
            for(int y = 0; y < yDim; y++) {
                sB.append(data[x][y]);
                sB.append("\t");
            }
            sB.setCharAt(sB.length() - 1, '\n');
        }
        return sB.toString();
    }

    /*
     *      equal if all elements are equal
     */
    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(!o.getClass().equals(this.getClass()))
            return false;
        Matrix rhs = (Matrix)o;
        if(xDim != rhs.xDim || yDim != rhs.yDim)
            return false;

        for(int x = 0; x < xDim; x++)
            for(int y = 0; y < yDim; y++)
                if(data[x][y] != rhs.data[x][y])
                    return false;
        return true;
    }
}
