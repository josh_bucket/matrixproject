package math.matrix;

public class MathErrorException extends Throwable {
    public MathErrorException(final String emsg){
        super(emsg);
    }
}
