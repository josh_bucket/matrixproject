package math.term;

import essential.Util;
import math.matrix.MathErrorException;
import math.matrix.Matrix;
import singeltons.MatrixList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public class TermInterpreter {
    private final String sourceTerm;
    private ArrayList<TermAtom> atoms;

    public TermInterpreter(final String term) throws MathErrorException, TermFormatException {
        //term potentially consist of numbers, matrix names and operators and dots and brackets
        sourceTerm = Util.ExtendedString.NonRegex.replaceAll(term, " ", "");
        atoms = new ArrayList<>();
        parseTerm();
        normaliseAtoms();
        evaluateAtoms();
    }

    /*
     *      if everything worked fine the result is the first and only element in atoms
     */
    public Matrix result() {
        if(atoms.size() > 1)
            return null;

        Optional<Matrix> res = atoms.get(0).asMatrix();
        if(res.isPresent())
            return res.get();
        return null;
    }

    /*
     *      replaces
     *      -(operator) with +(operator)-1(number)*(operator)
     *      /(operator)d(number) with *(operator)1/d(number) if d isnt null
     */
    private void normaliseAtoms() throws TermFormatException {
        int i;
        if(atoms.indexOf(new TermOperator("-")) == 0){
            atoms.remove(0);
            insertChainAtIndex(0, new TermAtom[]{ new TermMathElement(new Matrix(-1)), new TermOperator("*") });
        }
        while((i = atoms.indexOf(new TermOperator("-"))) > 0) {
            atoms.remove(i);
            insertChainAtIndex(i, new TermAtom[]{ new TermOperator("+"), new TermMathElement(new Matrix(-1)), new TermOperator("*") });
        }

        while((i = atoms.indexOf(new TermOperator("/"))) >= 0) {
            atoms.remove(i);
            if(atoms.size() == i)
                throw new TermFormatException("/ cant be the last character");

            TermAtom divisor = atoms.get(i);
            Double scalar = null;
            if(!(divisor.getClass().equals(TermMathElement.class) && (scalar = ((TermMathElement)divisor).val.tryGetScalar()) != null))
                throw new TermFormatException("divisor must be a real number");
            else if(scalar != 0){
                atoms.remove(i);
                insertChainAtIndex(i, new TermAtom[]{ new TermOperator("*"), new TermMathElement(new Matrix(1/scalar))});
            } else
                throw new TermFormatException("division by zero");
        }
    }

    /*
     *      inserts a chain of TermAtoms starting at i
     */
    private void insertChainAtIndex(int i, final TermAtom[] chain) {
        for(TermAtom a : chain)
            atoms.add(i++, a);
    }

    /*
     *      reduce the atoms by solving the term
     */
    private void evaluateAtoms() throws MathErrorException, TermFormatException {
        //operators are in the order of operation
        for (String operatorName : TermOperator.allowedOperators) {
            for (int i = 0; i < atoms.size(); i++) {
                TermAtom curr = atoms.get(i);
                //search for the right operator
                if (curr.equals(new TermOperator(operatorName))) {
                    TermAtom lhs = atoms.get(i - 1);
                    TermAtom rhs = atoms.get(i + 1);
                    //solve the operation
                    atoms.set(i, new TermMathElement(((TermOperator) curr).evaluate(lhs, rhs)));
                    atoms.remove(lhs);
                    atoms.remove(rhs);
                    i -= 2;
                }
            }
        }
    }

    /*
     *      fill the atoms of the term from string, either its a number, a variableName or an operator
     */
    private void parseTerm() throws MathErrorException, TermFormatException {
        for(int i = 0; i < sourceTerm.length();) {
            String currTerm = sourceTerm.substring(i);
            String relevantString = null;

            if(currTerm.startsWith("(")) {
                int close = Util.ExtendedString.findCorrespondingClosingIndex(currTerm, '(', ')');
                relevantString = currTerm.substring(1, close);
                TermInterpreter ti = new TermInterpreter(relevantString);
                Matrix res = ti.result();
                if(res == null)
                    throw new MathErrorException("faulty subterm");
                atoms.add(new TermMathElement(res));
                i += 2; //relevant string is two to short
            }else if((relevantString = Util.ExtendedString.startsWithNumber(currTerm)) != null) {
                atoms.add(new TermMathElement(new Matrix(Double.parseDouble(relevantString))));
            } else if ((relevantString = Util.ExtendedString.startsWithAny(currTerm, Arrays.stream(TermOperator.allowedOperators))) != null) {
                atoms.add(new TermOperator(relevantString));

            } else if ((relevantString = Util.ExtendedString.startsWithAny(currTerm, MatrixList.instance().keySet().stream())) != null) {
                atoms.add(new TermMathElement(MatrixList.instance().get(relevantString)));
            } else
                throw new TermFormatException("Invalid Input, unknown symbol: ..." + currTerm);

            i += relevantString.length();
        }
    }
}
