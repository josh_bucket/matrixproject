package math.term;

import math.matrix.Matrix;

public class TermMathElement extends TermAtom<Matrix> {

    TermMathElement(final Matrix val) { super(val); }
}
