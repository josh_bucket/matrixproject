package math.term;

import math.matrix.MathErrorException;
import math.matrix.Matrix;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TermOperator extends TermAtom<TermOperator.OperatorID> {

    enum OperatorID {
        pow ("^"),
        mult ("*"),
        plus ("+"),
        div("/"),
        minus("-");


        public final String mathematicalRepresentation;

        OperatorID(final String mathematicalRepresentation) {
            this.mathematicalRepresentation = mathematicalRepresentation;
        }

        public String getMathematicalRepresentation() {
            return mathematicalRepresentation;
        }

        /*
         *      Get the OperatorID from String
         */
        public static OperatorID mathematicalRepresentationToOperatorId(final String mathematicalRepresentation) {
            OperatorID oId = displayNameIndex.get(mathematicalRepresentation);
            if(oId == null)
                throw new IllegalArgumentException();
            return oId;
        }
    }

    private static final Map<String, OperatorID> displayNameIndex = Arrays.stream(OperatorID.values())
            .collect(Collectors.toMap(
                    OperatorID::getMathematicalRepresentation,
                    Function.identity())
            );

    /*
     *      elements need to be in the right order of operations (inverse operators "/", "-" could be in any order)
     */
    public final static String[] allowedOperators = {"^", "*", "/", "+", "-" };

    TermOperator(final String operatorName) {
        super(OperatorID.mathematicalRepresentationToOperatorId(operatorName));
    }

    /*
     *      "/" and "-" are supposed to be inverted away at this point
     *      gets the matrixes tests if they are 1x1 (a scalar) and them puts them in the right function to handle
     */
    public Matrix evaluate(final TermAtom lhs, final TermAtom rhs) throws TermFormatException, MathErrorException {

        Matrix lhsRaw;
        Matrix rhsRaw;
        if(lhs.asMatrix().isPresent() && rhs.asMatrix().isPresent()) {
            lhsRaw = (Matrix) lhs.asMatrix().get();
            rhsRaw = (Matrix) rhs.asMatrix().get();
        } else throw new TermFormatException("lhs and rhs are supposed to be Matrices");

        Double lhsScalar;
        Double rhsScalar;
        if((lhsScalar = lhsRaw.tryGetScalar()) != null) {
            if((rhsScalar = rhsRaw.tryGetScalar()) != null) {
                return scalarScalarOperations(lhsScalar, rhsScalar);
            } else {
                return scalarMatrixOperations(lhsScalar, rhsRaw);
            }
        } else {
            if((rhsScalar = rhsRaw.tryGetScalar()) != null) {
                return matrixScalarOperations(lhsRaw, rhsScalar);
            } else {
                return matrixMatrixOperations(lhsRaw, rhsRaw);
            }
        }
    }

    private Matrix scalarMatrixOperations(double scalar, Matrix matrix) throws TermFormatException, MathErrorException {
        switch (val) {
            case mult:
                return matrix.mult(scalar);
        }
        throw new TermFormatException("cannot perform operation: scalar X matrix");
    }

    private Matrix scalarScalarOperations(double lhs, double rhs) throws TermFormatException {
        switch (val) {
            case pow:
                return new Matrix(Math.pow(lhs, rhs));
            case mult:
                return new Matrix(lhs * rhs);
            case plus:
                return new Matrix(lhs + rhs);
        }
        throw new TermFormatException("cannot perform operation: scalar X scalar");
    }

    private Matrix matrixMatrixOperations(Matrix lhs, Matrix rhs) throws TermFormatException, MathErrorException {
        switch (val) {
            case mult:
                return lhs.mult(rhs);
            case plus:
                return lhs.add(rhs);
        }
        throw new TermFormatException("cannot perform operation: matrix X matrix");
    }

    private Matrix matrixScalarOperations(Matrix lhs, double rhs) throws TermFormatException, MathErrorException  {
        switch (val) {
            case pow:
                if (rhs != Math.floor(rhs) || Double.isInfinite(rhs) || Double.isNaN(rhs))
                    break;
                return lhs.pow((int)rhs);
            case mult:
                return lhs.mult(rhs);
        }
        throw new TermFormatException("cannot perform operation: matrix X scalar");
    }

    @Override
    public boolean equals(final Object o) {
        if(o == null)
            return false;
        if(!o.getClass().equals(this.getClass()))
            return false;
        return ((TermOperator)o).val == this.val;
    }
}
