package math.term;
import math.matrix.Matrix;

import java.util.Optional;

/*
 *      Container class for TermOperator or TermMathElement
 */
public class TermAtom<T> {
    protected T val;

    public TermAtom(final T val) {
        this.val = val;
    }

    public Optional<Matrix> asMatrix() {
        if(this.getClass().equals(TermMathElement.class))
            return Optional.of(((TermMathElement)this).val);
        return Optional.empty();
    }
}
