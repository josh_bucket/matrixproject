package math.term;


public class TermFormatException extends Throwable {
    public TermFormatException(final String emsg){
        super(emsg);
    }
}
