package essential;

/*
 *      Just a Container for two objects
 */
public class Pair<K, E> {
    public K left;
    public E right;

    public Pair(final K key, final E element) {
        this.left = key;
        this.right = element;
    }
}
