package essential;

public class Triple<L, M, R> {
    public L left;
    public M mid;
    public R right;

    public Triple(final L left, final M mid, final R right){
        this.left = left;
        this.mid = mid;
        this.right = right;
    }
}
