package essential;

import javax.swing.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

/*
 *      Extension functions
 */
public class Util {
    public static <R> Predicate<R> not(Predicate<R> predicate) {
        return predicate.negate();
    }

    public static File filePickerDialog(JFrame host) {
        JFileChooser fc = new JFileChooser();
        fc.showDialog(host, "OK");
        if (fc.getSelectedFile() != null)
            return fc.getSelectedFile();
        return null;
    }

    public static class ExtendedString {

        public static class NonRegex {

            public static String replaceAll(String source, String toReplace, String replacement) {

                StringBuilder retVal = new StringBuilder(source);
                int toReplaceLength = toReplace.length();
                for(int i = retVal.indexOf(toReplace); i != -1; i = retVal.indexOf(toReplace, i)) {
                    retVal.replace(i, i + toReplaceLength, replacement);
                }
                return retVal.toString();
            }

            /*
             * Doesn't return empty Strings, splits at any of the defined splitters
             */
            public static ArrayList<String> split(String source, char[] splitters) {

                ArrayList<String> retVal = new ArrayList<>();

                if(source == null || source.isEmpty())
                    return retVal;

                int stringStart = 0;
                int stringEnd = indexOfAny(source, splitters);

                while(stringEnd != -1) {
                    if(stringStart != stringEnd)
                        retVal.add(source.substring(stringStart, stringEnd));
                    stringStart = stringEnd + 1;
                    stringEnd = indexOfAny(source, splitters, stringStart);
                }

                if(stringStart != source.length())
                    retVal.add(source.substring(stringStart));

                return retVal;
            }

            /*
             *      finds the index of the next char in chars after startIndex
             *      @return the index of the char; -1 if no such char is found
             */
            public static int indexOfAny(String source, char[] chars, int startIndex) {

                Function<Character, Boolean> charsContains = c -> {
                    for(char curr : chars) if(c == curr) return true;
                    return false;
                };

                for(int i = startIndex; i < source.length(); i++)
                    if(charsContains.apply(source.charAt(i)))
                        return i;

                return -1;
            }

            public static int indexOfAny(String source, char[] chars) {
                return indexOfAny(source, chars, 0);
            }
        }

        /*
         *      Tests if string starts with any string in familiarStringStream
         *      @return the string if found, null if it doesnt start with a string from familiarStringStream
         */
        public static String startsWithAny(final String source, final Stream<String> familiarStringStream) {
            String[] result = familiarStringStream.filter(source::startsWith).toArray(String[]::new);
            return result.length > 0 ? result[0] : null;
        }

        /*
         *      Tests if string starts with any number
         *      @return the found number as string, null if there is no such number
         */
        public static String startsWithNumber(final String source){
            List<Character> allowed = Arrays.asList('0', '1','2','3','4','5','6','7','8','9','.');
            int i = 0;
            boolean isDouble = false;
            char currChar;
            while(i < source.length() && allowed.contains(currChar = source.charAt(i))) {
                if(currChar == '.')
                    if(isDouble)
                        break;
                    else
                        isDouble = true;

                i++;
            }
            return i == 0 ? null : source.substring(0, i);
        }

        /*
         *      finds the closing element to an opening element
         *      @return
         *              input "ddd((3dsad)dsad)dsad", '(', ')' returns the index of the last bracket
         *              input "ddd((3dsad)dsaddsad", '(', ')' returns -1
         */
        public static int findCorrespondingClosingIndex(final String source, char openChar, char closeChar) {
            int openCounter = 0;
            int i;
            for(i = source.indexOf(openChar); i < source.length(); i++) {
                char curr = source.charAt(i);
                if(curr == openChar)
                    openCounter++;
                else if(curr == closeChar)
                    openCounter--;
                if(openCounter == 0)
                    break;
            }
            if(i < source.length())
                return i;
            return -1;
        }
    }
}
