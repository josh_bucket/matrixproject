package essential;

import java.awt.*;
/*
 *      Essential colors
 */
public class Settings {
    public static final Color alertColor = Color.RED;
    public static final Color defaultFont = Color.BLACK;
    public static final Color defaultBackground = Color.lightGray;
}
